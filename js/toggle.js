let toggleCheckBox = document.getElementById("toggle-switch");
let priceBasic = document.getElementById("basic-price");
let pricePro = document.getElementById("pro-price");
let priceMaster = document.getElementById("master-price");


toggleCheckBox.addEventListener("change", function (event) {
    // console.log(event);
    if (this.checked) {
        console.log("checked");
        priceBasic.innerHTML = "&dollar; 19.99 ";
        pricePro.innerHTML = "&dollar; 24.99 ";
        priceMaster.innerHTML = "&dollar; 39.99 ";

    }
    else {
        console.log("unchecked");
        priceBasic.innerHTML = "&dollar; 199.99 ";
        pricePro.innerHTML = "&dollar; 249.99 ";
        priceMaster.innerHTML = "&dollar; 399.99 ";

    }
});